import React from 'react';
import logo from './logo.svg';

import HelloMessage from './components/HelloMessage';
import Timer from './components/Timer';

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <HelloMessage name="PLH"  />
        <Timer />
      </header>
    </div>
  );
}

export default App;
